<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    
    const HASH = "f67e57976317b9ecc117bb00dca725bd";
    
    public function page()
    {
         return view('addFile', ['headTitle' => 'Изменить данные']);
    }
    
    public function save(Request $request)
    {
        $pass = $request->input('password');

        if (md5($pass."lol") != IndexController::HASH) {
            return view("addFile",['result'=>false, 'msg'=>'Мы Вас не знаем!', 'headTitle' => 'Изменить данные']);
        } 
        
        $file = $_FILES['file'];

        if (isset($file) && ($file['error'] == 0)) {
                    
            // 15485760 bytes = 15 megabytes
            if ($file['size'] > 15485760) {
                return view("addFile",['result'=>false, 'response'=>"Файл слишком большой", 'headTitle' => 'Изменить данные']);
            }
            if ( ($file['type'] != 'application/vnd.ms-excel') && ($file['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')) {
                 return view("addFile",['result'=>false, 'msg'=>"Неизвестный формат, только файлы excel", 'headTitle' => 'Изменить данные']);
            }
            $type = explode('.', $file['name']);
            $type = array_pop($type);
                    
            $newPath =  getcwd() . '/public/uploads/uploads/files/';
            if(!is_dir($newPath)) {
                mkdir($newPath, 0777, true);
            }
            $fileNameFull = "current.".$type;
            $linkForDownload =  '/uploads/uploads/files/'.$fileNameFull;
            $fileNamFullPath = $newPath.$fileNameFull;
            move_uploaded_file($file['tmp_name'], $fileNamFullPath);   
                    
            $arr['data'] = [];
            $objPHPExcel = \PHPExcel_IOFactory::load($fileNamFullPath);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $worksheetTitle     = $worksheet->getTitle();
                $highestRow         = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;
                for ($row = 2; $row <= $highestRow; ++ $row) {
                    $a = [];
                    for ($col = 0; $col < $highestColumnIndex; ++ $col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        $dataType = \PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $a[] = $val;
                }
                $arr['data'][] = $a;

                }
                $myfile = fopen(getcwd() . '/public/excel/data.json', "w");
                fwrite($myfile, json_encode($arr));
                fclose($myfile);
                break;
           } 
        }
        return view('addFile', ['result'=>true, 'msg'=>'Успешно загружено!', 'headTitle' => 'Изменить данные']);
    }

}
