<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$app->get('/', function () use ($app) {
    return view('main',['headTitle'=>"Каталог акционных товаров"]);
});

$app->get('/buy', function () use ($app) {
    
    return view('page',['headTitle'=>"Где купить?", 
        'content'=> "<p style='padding:20px; font-size:13pt;'>".
        "Приобрести запасные части на автомобили Geely по акционным ценам Вы можете в магазине «АИС Автозапчасти», г. Киев, пер. Балтийский, 20 (магазин находится со стороны ул. Марка Вовчка).".
        "Информационная линия - 0800-508-540 (звонки по Украине со всех номеров бесплатные)..</p>".
        "<center><img src='/img/map.jpg'></center>"
        ]);
});

$app->get('/about', function () use ($app) {
    
    return view('page',['headTitle'=>"О компании",  'content'=>"<center><a href='http://ais.ua' target='_blank'>AIS</a></center>"]);
});

$app->get('/add-new-file', [
    'as' => 'index',
    'uses' => 'IndexController@page'
]);


$app->post('/save-file', [
    'as' => 'save',
    'uses' => 'IndexController@save'
]);
