<?php include("blocks/head.php"); ?>


    <!-- Main Content -->
    <div class="container">
        <style>
            p {margin:0 0 20px 0 !important; padding:10px;}
        </style>
 
                <?php if (isset($result) && $result): ?>
                    <p class="bg-success"><?=$msg?></p>
               <?php endif; ?>
                <?php if (isset($result) && !$result): ?>
                    <p class="bg-danger"><?=$msg?></p>
               <?php endif; ?>     
                <center>
                    <form method="post" action="/save-file" enctype="multipart/form-data">
                        
                        <div class="form-group">
                            <input type="file" name="file" value="Выберите файл (excel)" class="form-control" >
                          </div>
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Пароль" class="form-control" >
                        </div>
                       
                        <button type="submit" class="btn btn-success">Загрузить</button>
                         
                    </form>
                </center>

    </div>

    <hr>
    <?php include("blocks/footer.php")?>
