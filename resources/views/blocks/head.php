<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$headTitle?></title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="robots" content="INDEX,FOLLOW" />
<link rel="icon" href="http://ais.ua/media/favicon/default/favicon_2.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://ais.ua/media/favicon/default/favicon_2.ico" type="image/x-icon" />
<!--[if lt IE 7]>
<script type="text/javascript">
//<![CDATA[
    var BLANK_URL = 'http://ais.ua/js/blank.html';
    var BLANK_IMG = 'http://ais.ua/js/spacer.gif';
//]]>
</script>
<![endif]-->



<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/local.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
<script type="text/javascript" src="/js/controls.js"></script>
<script type="text/javascript" src="/js/slider.js"></script>

<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://ais.ua/skin/frontend/default/default/css/styles-ie.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://ais.ua/skin/frontend/default/ais-base/css/ie7.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://ais.ua/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://ais.ua/skin/frontend/base/default/js/ie6.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="http://ais.ua/skin/frontend/default/ais-base/css/ie8.css" media="all" />
<![endif]-->
<!--[if IE 9]>
<link rel="stylesheet" type="text/css" href="http://ais.ua/skin/frontend/default/ais-base/css/ie9.css" media="all" />
<![endif]-->

<body class=" cms-index-index cms-home">
<div class="wrapper">
        <div class="page">
        
<div class="header-container">
    <div id="contur">
        <div id="tt" style="background: none;">
            <div id="bb">
                <div id="ll">
                    <div id="rr" style="padding-top: 0;">
                        <div class="header-bkg">
							<div class="top-panel">
								<div style="    margin: 0 auto;font-size: 12pt; width: 630px; line-height: 45px;">Официальный дистрибьютор автомобилей и запасных частей на автомобили Geely</div>
							</div>
							<div class="header">
								<ul class="region-logo store-id-1">
																			<li class="first">
											
											<h1 class="logo"><strong></strong><a href="/" style="margin-left: -60px;" title="" class="logo"><img src="img/logo.png" width="80%" alt="" /></a></h1>
																					</li>
										<li class="storeVarLI" style="padding-right:50px !important; -webkit-box-sizing: initial;    box-sizing: initial;  padding-left:50px !important;    margin-top: 1px;  line-height: 28px;">
											<center>
												<h5 style="font-style: italic;  line-height: 28px;  font-size: 15pt;">Запасные части на автомобили Geely</h5>
											</center>
										</li>
																		<li class="last">
									   <!-- <img src="" alt="0 800 500 205" title="" /> -->
										<h1 id="ais-call-phone" class="ais-phone">0800-508- 540</h1><h5 class="ais-line-text">Информационная линия группы компаний АИС</h5>
																					
																			</li>
								</ul>
							</div>
																							<div class="banner-box-top">
									<link rel="stylesheet" type="text/css" href="http://ais.ua/skin/frontend/default/ais-base/unibanner/css/pulseblinds.css" />

<div id="wowslider-container" style="zoom:1;position: relative; max-width: 950px;margin-bottom:-17px;margin-top:-15px; z-index: 1; border: none;text-align: left;">
	<div class="ws_images">
		<ul>
			<li><img id="wows1_0" src="/img/geely_aktsia.jpg" title="" /></li>
	</ul>
	</div>
	<div class="ws_bullets"><div>
	
                                </div></div>
	<div class="ws_shadow"></div>
</div>


																	</div>
														<div class="nav-container">
    <ul id="nav">
        <table>
		<tbody>
		<tr>
		<td  class="level0 nav-1 first level-top">
		<li  class="level0 nav-1 first level-top">
		<a href="/"  class="level-top" >
		<span>Каталог запчастей</span></a></li>
                    
                <td  class="level0 nav-1 first level-top">
		<li  class="level0 nav-1 first level-top">
		<a href="/files/pricelist.xlsx"  class="level-top" >
		<span>Скачать прайс-лист</span></a></li>
                    
                    
                    <td  class="level0 nav-2 level-top">
		<li  class="level0 nav-2 level-top"><a href="/buy"  class="level-top" >
		<span>Где купить</span></a></li><td  class="level0 nav-3 level-top parent">
		<li  class="level0 nav-3 level-top parent"><a href="http://ais.ua" target="_blank"  class="level-top" >
		<span>О компании</span></a>
		</li>

		</tr></tbody></table>    </ul>
</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="lt">&nbsp;</div>
        <div class="rt">&nbsp;</div> -->
        <div class="rb">&nbsp;</div>
        <div class="lb">&nbsp;</div>
    </div>
</div>


<!--[if lt IE 8]>
<script type="text/javascript">
	jQuery('.nav-11').hover(function(){
			jQuery('.rb').addClass('hover');
		},
		function(){
			jQuery('.rb').removeClass('hover');
	})</script>
<![endif]-->
           
		   
		   
		   
		   
		   
		   <div class="main-container col1-layout">
            <div id="contur">
                <div id="tt">
                    <div id="bb">
                        <div id="ll">
                            <div id="rr">
                                <div class="main">
					                					                <div class="col-main" style="margin-top: -40px;">
<div class="page-title category-title">

    </div>
	<div>